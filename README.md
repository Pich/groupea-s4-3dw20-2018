# Débogage Back-end

Dépôt Framagit : https://framagit.org/Pich/groupea-s4-3dw20-2018.git

Avant toute action, il faut s'assurer que notre environnement est adéquate pour effectuer du débogage. Nous avons donc vérifié et, au besoin, configuré l'interpréteur PHP de façon à ce qu'il affiche toutes les erreurs :

```ini
error_reporting = E_ALL
display_errors = On
```

## Problème :  seules des erreurs PHP s'affichent sur la page

**Solution :**

Nous avons créé un projet l'IDE PHPStorm pour effectuer une première lecture du code. Le but est de réindenter le code
et de corriger les erreurs notifiées par le *linter* interne. De plus, à chaque erreur corrigée, l'on recharge la page pour
vérifier et poursuivre le débug.

Ce travail commence par le fichier `index.php`, puis cette procédure est appliquée aux autres fichiers selon les messages d'erreurs affichés dans le navigateur, tel que le template Twig `templates/index.tpl`.

**Le premier problème est résolu **

## Problème : le widget météo s'affiche mais des messages d'erreurs persistent

Ces message sont aussi bien dans la page que dans le widget lui-même.

 ````Notice: Undefined index: liste in D:\Code\Laragon\UE3DW20-S4\index.php on line 24````

**Solution :**

Ce type d'erreur signifie que le code essaye d'accéder à un index de tableau qui n'existe pas. Afin de résoudre le problème, nous devons savoir ce que le tableau contient. Pour ce faire, nous allons utiliser la fonction `var_dump()` sur le tableau pour en apprendre plus. Cette action nous a permis de corriger cette erreur.

Nous allons passer aux erreurs affichées dans le widget météo. Ces erreurs nous signalent que la fonction `mb_substr()` s'attend à recevoir une chaîne de caractères, mais c'est un tableau qui lui est donné. Le message référence notamment le fichier `lib/Twig/Extension/Core.php` et la fonction `twig_capitalize_string_filter()`. Or, cette fonction qui sert à mettre une chaîne de caractère en capitales est utilisée via l'alias `capitalize` dans les templates Twig. En corrigeant l'appel de la variable utilisée avec cet alias, nous avons résolu l'erreur. Cette action nous a permis de remédier aux erreurs dans le widget météo.

## Organisation du travail de groupe

Dans la continuité du travail de groupe de la semaine 3, la collaboration s'est organisée autour du [dépôt Framagit](https://framagit.org/Pich/groupea-s4-3dw20-2018.git) et du serveur Discord. Nos échanges sur ce serveur ont permis à Idriss de mieux appréhender Git et de lui donner la possibilité de contribuer au projet.

## Impression sur le travail de groupe travail de groupe (Driouch Idriss) 

Vincent Pichot a crée un dépôt Framagit et un serveur Discord pour organiser le travail sur ce projet. Dès le premier jour j'ai commencé. La semaine dernière, sincèrement, je n'ai rien fait : j'avais des problèmes pour m'adapter à cette formation, surtout que c'est ma première formation en développement web. 

Cette semaine, j'ai essayé de faire mon mieux, et grâce à l'aide de Vincent, j'ai pu commencer le travail. J'ai installé l'extension XDebug, et je l'ai mis en place dans l'IDE PHPStorm. Après, j'ai pu corrigé une partie des erreurs de syntaxe de la page `index.php`. 
Certes, je ne suis pas encore adapté avec le niveau de la formation, mais je progresse au fur et à mesure du temps.






